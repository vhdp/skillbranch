"use strict";

//import _ from 'lodash';
const _ = require('lodash');
// var regex = new RegExp('^[0-9А-яЁёA-z-@]+','ig');
// var nameArr = 'https://vk.com/tyui/skillbranch?w=wall-117903599_1076'.split('/');
// console.log(nameArr);
// var tempName = '';
// for (let i = nameArr.length-1; i>=0; i--) {
//   let name = nameArr[i].match( regex );
//   console.log(name);
//   if (name && name[0].indexOf('-') == -1 ) {
//   //   console.log(name);
//      tempName = name[0].replace(/@/g,'');
//      break;
//   }
// }
// let newName = '@'+tempName;
// console.log(newName);

/*
 board/
 board/vendor
 board/vendor/model/
 board/vendor/cpu
 board/vendor/cpu/
 board/vendor/cpu/model
 board/vendor/cpu/hz
 board/image
 board/video

 */
var rest = {
	"board": {
		"vendor": "IBM",
		"model": "IBM-PC S-100",
		"cpu": {"model": "80286", "hz": 12000},
		"image": "http://www.s100computers.com/My%20System%20Pages/80286%20Board/Picture%20of%2080286%20V2%20BoardJPG.jpg",
		"video": "http://www.s100computers.com/My%20System%20Pages/80286%20Board/80286-Demo3.mp4"
	},
	"ram": {"vendor": "CTS", "volume": 1048576, "pins": 30},
	"os": "MS-DOS 1.25",
	"floppy": 0,
	"hdd": [
		{"vendor": "Samsung",
			"size": 33554432,
			"volume": "C:"},
		{
		"vendor": "Maxtor",
		"size": 16777216,
		"volume": "D:"
	}, {
			"vendor": "Maxtor",
			"size": 8388608,
			"volume": "C:"}],
	"monitor": null,
	"length": 42,
	"height": 21,
	"width": 54
};

// function arrToObj(obj,parent) {
// 	let newObj = {};
// 	_.forEach(obj, function(value, key) {
// 		if (_.isString(value) || _.isNumber(value)) {
// 			if(!parrent) {
// 				newObj[key] = newObj[value];
// 			} else {
//
// 			}
//
// 		} else if(_.isPlainObject(value)) {
// 			arrToObj(value);
// 		}
// 	});
//
// }

let reqArr = [];
let objKeys = _.keys(rest);
let itemKey = null;

_.forEach(objKeys, function (value) {
	createReqPart(rest, value, null, 0)
});

function createReqPart(arr, key, parent, lvl) {
	let item = arr[key];
	let level = lvl;
	itemKey = parent ? `${parent}/${key}` : `/${key}`;

	if (_.isString(item) || _.isNumber(item) || _.isNull(item)) {
		reqArr.push(itemKey);
		itemKey = _.dropRight(itemKey.split('/'), level).join('/');
	} else if (_.isPlainObject(item)) {
		console.log(item);
		reqArr.push(itemKey);
		let keys = _.keys(item);

		_.forEach(keys, function (value, index) {
			if (index == keys.length - 1) {
				createReqPart(item, value, itemKey, keys.length);
			} else {
				createReqPart(item, value, itemKey, 1)
			}
		});
	} else if (_.isArray(item)) {
		reqArr.push(itemKey);
		let rootKey = itemKey;

		let objItem = _.zipObject(_.keys(item), item);
		//console.log(objItem);
		let newkeys = _.keys(objItem);

		_.forEach(newkeys, function (value, index) {
			if (index == 0) {
				createReqPart(objItem, value, '' + itemKey, 0);
			}
			else if (index == newkeys.length - 1) {
				createReqPart(objItem, value, rootKey + itemKey, newkeys.length);
			} else {
				createReqPart(objItem, value, rootKey + itemKey, 0);
			}
		});
	}

}
let requ =_.get(rest, _.drop(reqArr[0].split('/')));

console.log(requ);
console.log(reqArr);
let hddVol = rest.hdd;
let volumes = {};
_.forEach(hddVol, function (value, index) {
	volumes[value['volume']] = (volumes[value['volume']]) ? (Number(volumes[value['volume']].slice(0,-1))+ value['size'])+'B': value['size'] +'B';
});
console.log(volumes);