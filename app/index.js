import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import parsename from './midllwares/parseName';
const _ = require('lodash');
const errorhandler = require('errorhandler');
const fetch = require('node-fetch');


const app = express();


//const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
//
// let rest = {};
// fetch(pcUrl)
// 	.then(function(res) {
// 		return res.json();
// 	}).then(function(json) {
// 	rest = json;
// });
// console.log(rest);
var rest = {
	"board": {
		"vendor": "IBM",
		"model": "IBM-PC S-100",
		"cpu": {"model": "80286", "hz": 12000},
		"image": "http://www.s100computers.com/My%20System%20Pages/80286%20Board/Picture%20of%2080286%20V2%20BoardJPG.jpg",
		"video": "http://www.s100computers.com/My%20System%20Pages/80286%20Board/80286-Demo3.mp4"
	},
	"ram": {"vendor": "CTS", "volume": 1048576, "pins": 30},
	"os": "MS-DOS 1.25",
	"floppy": 0,
	"hdd": [
		{"vendor": "Samsung", 
			"size": 33554432, 
			"volume": "C:"}, 
		{
		"vendor": "Maxtor",
		"size": 16777216,
		"volume": "D:"
	}, 
		{"vendor": "Maxtor", 
			"size": 8388608, 
			"volume": "C:"}],
	"monitor": null,
	"length": 42,
	"height": 21,
	"width": 54
};

let reqArr = [];
let objKeys = _.keys(rest);
let itemKey = null;


_.forEach(objKeys, function (value) {
	createReqPart(rest, value, null, 0)
});

function createReqPart(arr, key, parent, lvl) {
	let item = arr[key];
	let level = lvl;
	itemKey = parent ? `${parent}/${key}` : `/${key}`;

	if (_.isString(item) || _.isNumber(item) || _.isNull(item)) {
		reqArr.push(itemKey);
		itemKey = _.dropRight(itemKey.split('/'), level).join('/');
	} else if (_.isPlainObject(item)) {
		reqArr.push(itemKey);
		let keys = _.keys(item);

		_.forEach(keys, function (value, index) {
			if (index == keys.length - 1) {
				createReqPart(item, value, itemKey, keys.length);
			} else {
				createReqPart(item, value, itemKey, 1)
			}
		});
	} else if (_.isArray(item)) {
		reqArr.push(itemKey);
		let rootKey = itemKey;

		let objItem = _.zipObject(_.keys(item), item);
		//console.log(objItem);
		let newkeys = _.keys(objItem);

		_.forEach(newkeys, function (value, index) {
			if (index == 0) {
				createReqPart(objItem, value, '' + itemKey, 0);
			}
			else if (index == newkeys.length - 1) {
				createReqPart(objItem, value, rootKey + itemKey, newkeys.length);
			} else {
				createReqPart(objItem, value, rootKey + itemKey, 0);
			}
		});
	}

}

app.use(morgan('dev'));


app.use(cors());

app.get('/', (req, res) => {
  res.json({
    hello: 'JS World'
  });
});
app.get('/homework', (req, res) => {
  res.redirect('/');
});
app.get('/homework/task2a', (req, res) => {
  let _query = req.query;
	if (Object.keys(_query).length == 0) {
		console.log('пуст');
		res.status(200).send('0');
	} else {
    let _a = req.query.a || 0;
    let _b = req.query.b || 0;
    let summ = +_a  + +_b;
    res.status(200).send(summ.toString());
  }
});
app.get('/homework/task2b', );

app.get('/homework/task2c', (req, res) => {
	let _query = req.query;
	var regex = new RegExp('^[0-9А-яЁёA-z-@.]+','ig');
	if (Object.keys(_query).length == 0 || !req.query.username) {
		console.log('Invalid fullname');
		res.status(200).send('Invalid fullname');
	} else {
		var nameArr = decodeURI(req.query.username).split('/');
		console.log(nameArr);
		var tempName = '';
		for (let i = nameArr.length-1; i>=0; i--) {
			let name = nameArr[i].match( regex );
			console.log(name);
			if (name && name[0].indexOf('-') == -1 ) {
				tempName = name[0].replace(/@/g,'');
				break;
			}
		}
		let newName = '@'+tempName;
		console.log(newName);
		res.status(200).send(newName);

	}
});
app.get('/homework/task3a/', (req, res) => {
	res.status(200);
	res.json(rest);
});
app.get('/homework/task3a/volumes', (req, res) => {
	let hddVol = rest.hdd;
	let volumes = {};
	_.forEach(hddVol, function (value, index) {
		volumes[value['volume']] = (volumes[value['volume']]) ? (Number(volumes[value['volume']].slice(0,-1))+ value['size'])+'B': value['size'] +'B';
	});
	res.status(200);
	res.json(volumes);
});
app.get('/homework/task3a/*', (req, res, next) => {
	let reqPath = req.path.replace('/homework/task3a','').replace(/\/$/,'');
	if(_.indexOf(reqArr, reqPath) == -1){
		var err = new Error();
		err.status = 404;
		next(err);
	} else  {
		res.status(200);
		res.json(_.get(rest, _.drop(reqArr[_.indexOf(reqArr, reqPath)].split('/'))));

	}

});


// app.get('*', function(req, res, next) {
// 	var err = new Error();
// 	err.status = 404;
// 	next(err);
// });


app.use(function(err, req, res, next) {
	// if (app.get('env') === 'development') {
	// 	return errorhandler(err, req, res, next);
	// } else
	if(err.status !== 404) {
		res.status(500).send('Server Error');
	} else {
		res.status(404).send('Not Found');
	}
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
