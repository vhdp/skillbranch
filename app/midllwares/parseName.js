export function parseName(req,res) {
	let _query = req.query;
	let regex = new RegExp('^[\u00C0-\u017FА-яЁёA-z ]+$','ig');
	if (Object.keys(_query).length == 0 || !req.query.fullname) {
		console.log('Invalid fullname');
		res.status(200).send('Invalid fullname');
	} else {
		let nameArr = decodeURI(req.query.fullname).split(' ');

		console.log(nameArr);
		if (nameArr.length > 3 || !regex.test(decodeURI(req.query.fullname)) || decodeURI(req.query.fullname).indexOf("_") >=0) {
			res.status(200).send('Invalid fullname');
		}else {
			let surName = nameArr[nameArr.length - 1];
			let newName = (nameArr[nameArr.length - 3]) ? ' ' + nameArr[nameArr.length - 3].charAt(0).toUpperCase() + '.' : '';
			let newMname = (nameArr[nameArr.length - 2]) ? ' ' + nameArr[nameArr.length - 2].charAt(0).toUpperCase() + '.' : '';
			res.status(200).send(surName + newName + newMname);
		}
	}
}