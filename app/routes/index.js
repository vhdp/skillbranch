module.exports = function(app) {

	app.get('/translate', require('./translate').get);
	app.post('/translate', require('./translate').post);

	app.get('/news', require('./news').get);
	//app.post('/news', require('./news').post);

	app.get('/', require('./root').get);

};